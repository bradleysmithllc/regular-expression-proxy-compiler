package org.bitbucket.bradleysmithllc.regular_expression_proxy_compiler.test;

import org.bitbucket.bradleysmithllc.regular_expression_proxy_compiler.ParameterExpression;
import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParameterExpressionTest {
	@Test
	public void testExpression()
	{
		ParameterExpression pe = new ParameterExpression("(?{Hello})");

		Assert.assertTrue(pe.matches());
		Assert.assertEquals("Hello", pe.getParameterName());

		pe = new ParameterExpression("\\(\\?\\{(?'parameterName'[^\\}]+)\\}\\)(?{Param})");

		Assert.assertTrue(pe.hasNext());
		Assert.assertEquals("Param", pe.getParameterName());
	}

	@Test
	public void testSpaceyExpression()
	{
		ParameterExpression pe = new ParameterExpression("( ? { Hello \r\r}\n\n )");

		Assert.assertTrue(pe.matches());
		Assert.assertEquals("Hello", pe.getParameterName());
	}

	@Test
	public void testTestExpressionReplace()
	{
		Pattern p = Pattern.compile("AA A");
		Matcher m = p.matcher("BAA ABJJJJJJJAA AU");

		Assert.assertTrue(m.find());
		System.out.println(m.group(0));
	}
/*
	@Test
	public void testTestExpression()
	{
		ParameterTestExpression pe = new ParameterTestExpression("Hello Mike Brown", "Mike", "Brown");

		Assert.assertTrue(pe.matches());
		Assert.assertEquals("Mike", pe.getParamParameter());
		Assert.assertEquals("Brown", pe.getParam2Parameter());
	}
*/
}