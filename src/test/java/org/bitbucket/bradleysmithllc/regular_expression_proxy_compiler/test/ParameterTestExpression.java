package org.bitbucket.bradleysmithllc.regular_expression_proxy_compiler.test;

import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public final class ParameterTestExpression
{
	public static final String PATTERN_RAW_TEXT = "(?'name'Hello (?{Param}) ( ? { Param2 } ))";
	private final String parameter_Param;
	private final String parameter_Param2;
	public final String pattern_text;
	public final Pattern pattern;
	private final Map<String, Integer> groupOffsets = new HashMap<String, Integer>();

	private final Matcher matcher;
	private final CharSequence matchText;

	public static interface RegExpIterator
	{
		String replaceMatch(ParameterTestExpression e);
	}


	public ParameterTestExpression(CharSequence ch, String Param, String Param2)
	{
		String s_pattern_text = PATTERN_RAW_TEXT.replaceAll("\\(\\?'[^']+'", "(");

		//Param >> (?{Param});
		parameter_Param = Param;

		s_pattern_text = s_pattern_text.replace("(?{Param})", parameter_Param);

		//Param2 >> ( ? { Param2 } );
		parameter_Param2 = Param2;

		s_pattern_text = s_pattern_text.replace("( ? { Param2 } )", parameter_Param2);

		pattern_text = s_pattern_text;

		Pattern cpattern = Pattern.compile("(\\\\)?\\((\\?'([^']+)')?");

		Matcher m = cpattern.matcher(PATTERN_RAW_TEXT);

		int groupCount = 1;

		while (m.find())
		{
			if (m.group(1) != null)
			{
				continue;
			}

			String groupName = m.group(3);

			if (groupName != null)
			{
				groupOffsets.put(groupName, new Integer(groupCount));
			}

			groupCount++;
		}

		pattern = Pattern.compile(pattern_text, Pattern.CASE_INSENSITIVE);

		matchText = ch;
		matcher = pattern.matcher(matchText);
	}

	public static ParameterTestExpression match(CharSequence pText, String Param, String Param2)
	{
		return new ParameterTestExpression(pText, Param, Param2);
	}

	public int end()
	{
		return matcher.end();
	}

	public int end(int group)
	{
		return matcher.end(group);
	}

	public int start()
	{
		return matcher.start();
	}

	public int start(int group)
	{
		return matcher.start(group);
	}

	public String replaceAll(String replacement)
	{
		return matcher.replaceAll(replacement);
	}

	public String replaceAll(RegExpIterator it)
	{
		String match = matchText.toString();
		String str = "";
		int end = -1;

		while (hasNext())
		{
			str += match.substring(end == -1 ? 0 : end, start());
			str += it.replaceMatch(this);
			end = end();
		}

		str += match.substring(end == -1 ? 0 : end, match.length());
		return str;
	}

	public boolean matches()
	{
		return matcher.matches();
	}

	public boolean hasNext()
	{
		return matcher.find();
	}

	public int groupCount()
	{
		return matcher.groupCount();
	}

	public String group()
	{
		return matcher.group();
	}

	public String group(int i)
	{
		return matcher.group(i);
	}

	public ParameterTestExpression resetMatch(CharSequence seq, String Param, String Param2)
	{
		return match(seq, Param, Param2);
	}

	public String group(String name)
	{
		return matcher.group(groupOffsets.get(name).intValue());
	}

	private static String scope(String text, String name)
	{
		return text.replaceAll("(\\(\\?')(\\w+')", "$1" + name + ".$2");
	}

	public final String getParamParameter()
	{
		return parameter_Param;
	}

	public final String getParam2Parameter()
	{
		return parameter_Param2;
	}


	public boolean hasName()
	{
		return group("name") != null;
	}

	public String getName()
	{
		if (!hasName())
		{
			throw new IllegalArgumentException("Property not defined: Name");
		}
		return group("name");
	}

	public String toString()
	{
		return "{ParameterTestExpression, matchText='" + matchText + "'}[" + "(name)=" + (hasName() ? getName() : "null") + "]";
	}

	public static void main(String [] argv)
	{
		System.out.println(PATTERN_RAW_TEXT);
	}
}