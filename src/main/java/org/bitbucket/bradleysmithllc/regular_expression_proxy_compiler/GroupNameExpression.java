package org.bitbucket.bradleysmithllc.regular_expression_proxy_compiler;

import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public final class GroupNameExpression
{
	public static final String PATTERN_RAW_TEXT = "(?'escape'\\\\)?\\((\\?'(?'groupName'[^']+)')";
	public static final String PATTERN_TEXT;
	public static final Pattern pattern;
	private static final Map<String, Integer> groupOffsets = new HashMap<String, Integer>();

	private final Matcher matcher;
	private final CharSequence matchText;

	public static interface RegExpIterator
	{
		String replaceMatch(GroupNameExpression e);
	}

	static
	{
		PATTERN_TEXT = PATTERN_RAW_TEXT.replaceAll("\\(\\?'[^']+'", "(");

		Pattern cpattern = Pattern.compile("(\\\\)?\\((\\?'([^']+)')?");

		Matcher matcher = cpattern.matcher(PATTERN_RAW_TEXT);

		int groupCount = 1;

		while (matcher.find())
		{
			if (matcher.group(1) != null)
			{
				continue;
			}

			String groupName = matcher.group(3);

			if (groupName != null)
			{
				groupOffsets.put(groupName, new Integer(groupCount));
			}

			groupCount++;
		}

		pattern = Pattern.compile(PATTERN_TEXT, Pattern.CASE_INSENSITIVE);
	}


	private GroupNameExpression(Matcher pMatcher, CharSequence ch)
	{
		matcher = pMatcher;
		matchText = ch;
	}

	public GroupNameExpression(CharSequence ch)
	{
		this(pattern.matcher(ch), ch);
	}

	public static GroupNameExpression match(CharSequence pText)
	{
		return new GroupNameExpression(pText);
	}

	public int end()
	{
		return matcher.end();
	}

	public int end(int group)
	{
		return matcher.end(group);
	}

	public int start()
	{
		return matcher.start();
	}

	public int start(int group)
	{
		return matcher.start(group);
	}

	public String replaceAll(String replacement)
	{
		return matcher.replaceAll(replacement);
	}

	public String replaceAll(RegExpIterator it)
	{
		String match = matchText.toString();
		String str = "";
		int end = -1;

		while (hasNext())
		{
			str += match.substring(end == -1 ? 0 : end, start());
			str += it.replaceMatch(this);
			end = end();
		}

		str += match.substring(end == -1 ? 0 : end, match.length());
		return str;
	}

	public boolean matches()
	{
		return matcher.matches();
	}

	public boolean hasNext()
	{
		return matcher.find();
	}

	public int groupCount()
	{
		return matcher.groupCount();
	}

	public String group()
	{
		return matcher.group();
	}

	public String group(int i)
	{
		return matcher.group(i);
	}

	public GroupNameExpression resetMatch(CharSequence seq)
	{
		return match(seq);
	}

	public String group(String name)
	{
		return matcher.group(groupOffsets.get(name).intValue());
	}

	private static String scope(String text, String name)
	{
		return text.replaceAll("(\\(\\?')(\\w+')", "$1" + name + ".$2");
	}


	public boolean hasEscape()
	{
		return group("escape") != null;
	}

	public String getEscape()
	{
		if (!hasEscape())
		{
			throw new IllegalArgumentException("Property not defined: Escape");
		}
		return group("escape");
	}

	public boolean hasGroupName()
	{
		return group("groupName") != null;
	}

	public String getGroupName()
	{
		if (!hasGroupName())
		{
			throw new IllegalArgumentException("Property not defined: GroupName");
		}
		return group("groupName");
	}

	public String toString()
	{
		return "{GroupNameExpression, matchText='" + matchText + "'}[" + "(escape)=" + (hasEscape() ? getEscape() : "null") + ", (groupName)=" + (hasGroupName() ? getGroupName() : "null") + "]";
	}

	public static void main(String [] argv)
	{
		System.out.println(PATTERN_TEXT);
	}
}