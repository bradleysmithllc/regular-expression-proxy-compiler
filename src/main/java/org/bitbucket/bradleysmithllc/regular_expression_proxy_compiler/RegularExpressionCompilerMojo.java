package org.bitbucket.bradleysmithllc.regular_expression_proxy_compiler;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;

@Mojo(name = "generate-regular-expression-proxies", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class RegularExpressionCompilerMojo
	extends AbstractMojo {
	@Parameter(property = "project", defaultValue = "${project}")
	protected MavenProject mavenProject;

	@Parameter(property = "outputDirectory", defaultValue = "${project.build.directory}/generated-sources/regexp")
	protected File outputDirectory;

	@Parameter(property = "regexpSourceDirectory", defaultValue = "${basedir}/src/main/regexp")
	protected File regexpSourceDirectory;

	public void execute()
		throws MojoExecutionException {
		outputDirectory.mkdirs();
		regexpSourceDirectory.mkdirs();

		try {
			RegularExpressionProxyClassGenerator.generateProxyFromPropertiesDirectory(regexpSourceDirectory, outputDirectory);
		} catch (Exception e) {
			getLog().error(e);
		}

		mavenProject.addCompileSourceRoot(outputDirectory.getAbsolutePath());
	}
}